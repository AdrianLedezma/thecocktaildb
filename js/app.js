

    const formulario = document.getElementById('coctelFormulario');
    const nombreCoctel = document.getElementById('inpNombre');
    const infoCoctel = document.getElementById('Informacion');
    const btnLimpiar = document.getElementById('btnLimpiar');



    formulario.addEventListener('submit', async (formulario) => {
        formulario.preventDefault();
        const nomCoctel = nombreCoctel.value.trim();
        if (!nomCoctel) {
            alert('Por favor, ingresa el nombre del cóctel.');
            return;
        }
        try {
            const respuesta = await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=`+nomCoctel);
            const coctel = respuesta.data.drinks[0];
            if (!coctel) {
                alert('No se encontro el cóctel.');
            }
            MostrarInformacion(coctel);
            } 
            catch (error) {
                alert('Lo sentimos, el cóctel no fue encontrado. Por favor inténtalo de nuevo.');
            }
    });

    function MostrarInformacion(coctel) {
        infoCoctel.innerHTML = `
            <p>Categoria: ` + coctel.strCategory +`</p>
            <p>Instrucciones: ` + coctel.strInstructions +`</p>
            <center><p>Imagen</p>
            <img id="imagenCoctel" src="` + coctel.strDrinkThumb +`" alt="Imagen del cóctel"> </center>
        `;
    }
    btnLimpiar.addEventListener('click', () => {
        LimpiarInformacion();
    });

    function LimpiarInformacion() {
        infoCoctel.innerHTML = `
            <p>Categoria:</p>
            <p>Instrucciones:</p>
            <center><p>Imagen</p>
            <img id="imagenCoctel" src="" alt="Imagen del cóctel"> </center>
        `;
        document.getElementById("inpNombre").value = "";
    }
